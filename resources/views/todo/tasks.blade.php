@extends('layouts.app')

@section('content')
    <div class="container">
        <assigned-tasks-component assigned-tasks-load-route="{{ route('assignedTasksLoad') }}" ></assigned-tasks-component>
    </div>
@endsection
