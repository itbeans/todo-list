@extends('layouts.app')

@section('content')
    <div class="container">
    <todo-component fetch-todo-route="{{ route('fetchTodo') }}"></todo-component>
    </div>
@endsection
