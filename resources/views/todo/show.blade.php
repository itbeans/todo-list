@extends('layouts.app')

@section('content')
    <div class="container">
        <view-todo-component fetch-data-route="{{ route('fetchTodoData',$id) }}" ></view-todo-component>
    </div>
@endsection
