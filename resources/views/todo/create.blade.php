@extends('layouts.app')

@section('content')
    <div class="container">
        <add-todo-component  add-todo-route="{{ route('addTodo') }}"  fetch-team-route="{{ route('fetchTeam') }}" edit-id="0"></add-todo-component>
    </div>

@endsection
