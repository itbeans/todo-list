@component('mail::message')
# Introduction

<h3>{{$title}}</h3>
<div>
    {{$details}}
</div>
{{--@component('mail::button', ['url' => ''])--}}
{{--Button Text--}}
{{--@endcomponent--}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
