@extends('layouts.app')

@section('content')
    <div class="container">
        <reminder-component fetch-reminder-route="{{ route('fetchReminder') }}"></reminder-component>
    </div>
@endsection
