@extends('layouts.app')

@section('content')
    <div class="container">
        <add-reminder-component
            fetch-todo-route="{{ route('fetchTodo') }}"
            add-reminder-route="{{route('addReminder')}}">

        </add-reminder-component>
    </div>

@endsection
