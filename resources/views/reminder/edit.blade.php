@extends('layouts.app')

@section('content')
    <home-component
        fetch-todo-route="{{ route('fetchTodo') }}"
        fetch-rem-route="{{ route('fetchReminder') }}"
        add-todo-route="{{ route('addTodo') }}">

    </home-component>
@endsection
