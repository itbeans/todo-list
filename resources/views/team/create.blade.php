@extends('layouts.app')

@section('content')
    <div class="container">
        <create-team-component  add-team-route="{{ route('addTeam') }}" load-user-roles-route="{{route('getAllRoles')}}" edit-id="0"></create-team-component>
    </div>

@endsection
