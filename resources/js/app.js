/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
// Main Home Component
Vue.component('home-component', require('./components/homeComponent.vue').default);
// Todo Components
Vue.component('todo-component', require('./components/todo/TodoComponent.vue').default);
Vue.component('view-todo-component',require('./components/todo/ViewTodoComponent.vue').default);
Vue.component('add-todo-component', require('./components/todo/AddTodoComponent.vue').default);
Vue.component('add-todo-component', require('./components/todo/AddTodoComponent.vue').default);
Vue.component('assigned-tasks-component', require('./components/todo/AssignedTasksComponent.vue').default);
// Reminder Components
Vue.component('reminder-component', require('./components/reminder/ReminderComponent.vue').default);
Vue.component('view-reminder-component', require('./components/reminder/ViewReminderComponent.vue').default);
Vue.component('add-reminder-component', require('./components/reminder/AddReminderComponent.vue').default);


// Reminder Components
Vue.component('create-team-component', require('./components/team/CreateTeamComponent.vue').default);
// Vue.component('edit-team-component', require('./components/reminder/ViewReminderComponent.vue').default);
Vue.component('team-component', require('./components/team/TeamComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
