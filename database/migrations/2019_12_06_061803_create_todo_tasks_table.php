<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTodoTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('todo_tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->time('at_time')->nullable()->default(null);
            $table->string('title')->nullable()->default(null);
            $table->text('details')->nullable()->default(null);
            $table->integer('todo_id');
            $table->integer('max_min');
            $table->unsignedInteger('user_id')->default(null);
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('todo_tasks');
    }
}
