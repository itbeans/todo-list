<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRemindersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reminders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable()->default(null);
            $table->text('details')->nullable()->default(null);
            $table->integer('status')->nullable()->default(0);
            $table->integer('user_id');
            $table->integer('todo_id');
            $table->integer('lets_process')->nullable()->default(0);
            $table->timestamps();

//            $table->foreign('reminder_type_id')->references('id')->on('reminder_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reminders');
    }
}
