<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReminderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reminder_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('at_date');
            $table->time('at_time');
            $table->integer('is_done')->default(0);
            $table->unsignedInteger('reminder_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reminder_details');
    }
}
