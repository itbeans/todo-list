<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * HOW TO RUN Single Seeder
         *  php artisan db:seed --class=Seeder Name
         * */

        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        //        Create User Roles
        $super_admin=Role::create(['name'=>'super admin']);
        $super_editor=Role::create(['name'=>'super editor']);
        $super_viewer=Role::create(['name'=>'super viewer']);
        $super_writer=Role::create(['name'=>'super writer']);
//
        $todo_admin=Role::create(['name'=>'todo admin']);
        $todo_editor=Role::create(['name'=>'todo editor']);
        $todo_viewer=Role::create(['name'=>'todo viewer']);
        $todo_writer=Role::create(['name'=>'todo writer']);
//
        $reminder_admin=Role::create(['name'=>'reminder admin']);
        $reminder_editor=Role::create(['name'=>'reminder editor']);
        $reminder_viewer=Role::create(['name'=>'reminder viewer']);
        $reminder_writer=Role::create(['name'=>'reminder writer']);
//
        $team_admin=Role::create(['name'=>'team admin']);
        $team_editor=Role::create(['name'=>'team editor']);
        $team_viewer=Role::create(['name'=>'team viewer']);
        $team_writer=Role::create(['name'=>'team writer']);

        //        Create Permissions
        $todo_create= Permission::create(['name'=>'create todo']);
        $todo_update= Permission::create(['name'=>'edit todo']);
        $todo_view= Permission::create(['name'=>'view todo']);
        $todo_delete= Permission::create(['name'=>'delete todo']);

        $reminder_create= Permission::create(['name'=>'create reminder']);
        $reminder_update= Permission::create(['name'=>'edit reminder']);
        $reminder_view= Permission::create(['name'=>'view reminder']);
        $reminder_delete= Permission::create(['name'=>'delete reminder']);

        $team_create= Permission::create(['name'=>'create team']);
        $team_update= Permission::create(['name'=>'edit team']);
        $team_view= Permission::create(['name'=>'view team']);
        $team_delete= Permission::create(['name'=>'delete team']);


        //        Assign Permissions to roles
        $super_admin->syncPermissions([
            $todo_create,$todo_update,$todo_view,$todo_delete,
            $reminder_create, $reminder_update,$reminder_view,$reminder_delete,
            $team_create,$team_update,$team_view,$team_delete
        ]);
        $super_editor->syncPermissions([
            $todo_create,$todo_update,$todo_view,
            $reminder_create,$reminder_update,$reminder_view,
            $team_create,$team_update,$team_view
        ]);
        $super_viewer->syncPermissions([
            $todo_view,
            $reminder_view,
            $team_view
        ]);
        $super_writer->syncPermissions([
            $todo_create,$todo_update,$todo_view,$todo_delete,
            $reminder_create, $reminder_update,$reminder_view,$reminder_delete,
            $team_create,$team_update,$team_view,$team_delete
        ]);

        $todo_admin->syncPermissions([
            $todo_create,$todo_update,$todo_view,$todo_delete
        ]);
        $todo_editor->syncPermissions([
            $todo_create,$todo_update,$todo_view,
        ]);
        $todo_viewer->syncPermissions([
            $todo_view
        ]);
        $todo_writer->syncPermissions([
            $todo_create,$todo_update,$todo_view,$todo_delete
        ]);
//
        $reminder_admin->syncPermissions([
            $reminder_create, $reminder_update,$reminder_view,$reminder_delete
        ]);
        $reminder_editor->syncPermissions([
            $reminder_create,$reminder_update,$reminder_view
        ]);
        $reminder_viewer->syncPermissions([
            $reminder_view
        ]);
        $reminder_writer->syncPermissions([
            $reminder_create,$reminder_update,$reminder_view
        ]);

        //
        $team_admin->syncPermissions([
            $team_create,$team_update,$team_view,$team_delete
        ]);
        $team_editor->syncPermissions([
            $team_create,$team_update,$team_view
        ]);
        $team_viewer->syncPermissions([
            $team_view
        ]);
        $team_writer->syncPermissions([
            $team_create,$team_update,$team_view,$team_delete
        ]);
    }
}
