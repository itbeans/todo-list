<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
/*
 * TODO ROUTING GROUP
 * */
//TodoMiddleware
Route::group(['middleware'=>'auth'],function(){

Route::group(['prefix' => 'todo','middleware'=>'_todo'],function(){
    Route::get('/', 'TodoController@todoIndex')->name('todos');
    Route::get('/create', 'TodoController@create')->name('createTodo');
    Route::get('/show/{id}', 'TodoController@todoShow');
    Route::get('/fetch-todo-data/{todo}', 'TodoController@show')->name("fetchTodoData");
    Route::get('/destroy/{todo}', 'TodoController@destroy');
    Route::get('/fetch-todos', 'TodoController@index')->name("fetchTodo");

    Route::post('/', 'TodoController@store')->name('addTodo');

    Route::get('/tasks', 'TodoTaskController@tasksIndex')->name('tasks');
    Route::get('/assigned-tasks-load', 'TodoTaskController@index')->name('assignedTasksLoad');
    Route::get('/taskDone/{todoTask}', 'TodoTaskController@update');
});
// end Common User Routing Group

/*
 * REMINDER ROUTING GROUP 'middleware'=>'todoMiddleware'
 * */
Route::group(['prefix' => 'reminder','middleware'=>'_reminder'],function(){
    Route::get('/', 'ReminderController@reminderIndex')->name('reminders');
    Route::get('/create', 'ReminderController@create')->name('createReminder');
    Route::get('/show/{id}', 'ReminderController@reminderShow');
    Route::get('/fetch-reminder-data/{reminder}', 'ReminderController@show')->name("fetchReminderData");
    Route::get('/destroy/{reminder}', 'ReminderController@destroy');
    Route::get('/fetch-reminders', 'ReminderController@index')->name("fetchReminder");

    Route::post('/', 'ReminderController@store')->name('addReminder');

});
// end Common User Routing Group

/*
 * Team ROUTING GROUP
 * */
Route::group(['prefix' => 'team' ,'middleware'=>'_team'],function(){
    Route::get('/', 'TeamController@teamIndex')->name('team');
    Route::get('/create', 'TeamController@create')->name('addTeamMember');
    Route::get('/show/{id}', 'TeamController@teamShow');
    Route::get('/fetch-team-data/{team}', 'TeamController@show')->name("fetchTeamData");
    Route::get('/destroy/{team}', 'TeamController@destroy');
    Route::get('/fetch-team', 'TeamController@index')->name("fetchTeam");

    Route::post('/', 'TeamController@store')->name('addTeam');
});
// end Common User Routing Group

Route::get('/get-all-roles', 'RolesAndPermissionsController@index')->name('getAllRoles');


});
