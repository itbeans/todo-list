<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class TeamMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->is('team/create')){
            if(!Auth::user()->hasPermissionTo('create team')){
                dd("You Dont Have Permission For That");
            }
        }
        elseif($request->is('team')){
            if(!Auth::user()->hasPermissionTo('view team')){
                dd("You Dont Have Permission For That");
            }
        }elseif($request->is('team/destroy/')){
            if(!Auth::user()->hasPermissionTo('delete team')){
                dd("You Dont Have Permission For That");
            }
        }
        return $next($request);
    }
}
