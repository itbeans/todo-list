<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class TodoMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->is('todo/create')){
            if(!Auth::user()->hasPermissionTo('create todo')){
                dd("You Dont Have Permission For That");
            }
        }
        elseif($request->is('todo')){
            if(!Auth::user()->hasPermissionTo('view todo')){
                dd("You Dont Have Permission For That");
            }
        }elseif($request->is('todo/destroy/')){
            if(!Auth::user()->hasPermissionTo('delete todo')){
                dd("You Dont Have Permission For That");
            }
        }
        return $next($request);

    }
}
