<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class ReminderMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->is('reminder/create')){
            if(!Auth::user()->hasPermissionTo('create reminder')){
                dd("You Dont Have Permission For That");
            }
        }
        elseif($request->is('reminder')){
            if(!Auth::user()->hasPermissionTo('view reminder')){
                dd("You Dont Have Permission For That");
            }
        }elseif($request->is('reminder/destroy/')){
            if(!Auth::user()->hasPermissionTo('delete reminder')){
                dd("You Dont Have Permission For That");
            }
        }
        return $next($request);
    }
}
