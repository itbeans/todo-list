<?php

namespace App\Http\Controllers;

use App\Reminder;
use Illuminate\Http\Request;
use Illuminate\Contracts\Validation\Validator;

class ReminderController extends Controller
{
    protected $response=array("status"=>true,"result"=>"","message"=>"",'errors'=>'');

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'main'=>['required','array'],
            'main.title' => ['required', 'string'],
            'main.details' => [],
            'main.todo_id' => ['required'],//, 'confirmed' for conifrm pass
            'dataRows' => ['required','array','min:1'],
            'dataRows.at_date*'=>['required'],
            'dataRows.at_time*'=>['required']
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->response['result']=auth()->user()->reminders;
        return response(json_encode($this->response));
    }
    public function reminderIndex()
    {
        return view('reminder.index');

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('reminder.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData=$request->all()['data'];
        $requestData=$this->validator($requestData);
        if($requestData->fails()){
            return  response(json_encode($requestData->errors()),'422');
        }
        else{
            $requestData=$requestData->validated();
        }
        $reminder=auth()->user()->reminders()->create($requestData['main']);
        $reminder->reminder_details()->createMany($requestData['dataRows']);
        return response()->json($this->response);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reminder  $reminder
     * @return \Illuminate\Http\Response
     */
    public function show(Reminder $reminder)
    {
        $this->response['result']=array('reminder'=>$reminder,'todos'=>$reminder->todo);
        return response(json_encode($this->response));
    }
    public function reminderShow($id){
        return view('reminder.show',compact('id'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reminder  $reminder
     * @return \Illuminate\Http\Response
     */
    public function edit(Reminder $reminder)
    {
        $this->response['result']=array('reminder'=>$reminder,'todo'=>$reminder->todo);
        return response(json_encode($this->response));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reminder  $reminder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $requestData=$request->all()['data'];
        $reminder=Reminder::find($request['id']);
        $reminder->update($requestData);
        $this->response['message']="Update Successfull";
        return response(json_encode($this->response));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reminder  $reminder
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reminder $reminder)
    {
        $reminder->delete();
//        return redirect()->route('users.index')
//            ->with('flash_message',
//                'User successfully deleted.');
        return back();
    }
}
