<?php

namespace App\Http\Controllers;

use App\Mail\ReminderEmail;
use App\Reminder;
use App\ReminderDetail;

use App\Todo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     */

    public function index()
    {
        return view('home');
    }

}
