<?php

namespace App\Http\Controllers;

use App\TodoTask;
use Illuminate\Http\Request;

class TodoTaskController extends Controller
{
    protected $response=array("status"=>true,"result"=>"","message"=>"",'errors'=>'');
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->response['result']=auth()->user()->tasks;
        return response(json_encode($this->response));
    }
    public function tasksIndex()
    {
        return view('todo.tasks');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(array $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TodoTask  $todoTask
     * @return \Illuminate\Http\Response
     */
    public function show(TodoTask $todoTask)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TodoTask  $todoTask
     * @return \Illuminate\Http\Response
     */
    public function edit(TodoTask $todoTask)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TodoTask  $todoTask
     * @return \Illuminate\Http\Response
     */
    public function update(TodoTask $todoTask)
    {
        $todoTask->status=3;
        $todoTask->save();
        return back()->with('flash_message','Task Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TodoTask  $todoTask
     * @return \Illuminate\Http\Response
     */
    public function destroy(TodoTask $todoTask)
    {
        //
    }
}
