<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class TeamController extends Controller
{
    protected $response=array("status"=>true,"result"=>"","message"=>"",'errors'=>'');
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return \Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],//, 'confirmed' for conifrm pass
            'user_role' => ['required'],
        ]);
    }
    public function index()
    {
        $this->response['result']= auth()->user()->team()->with(['roles'=>function($query){
            $query->select('name');
        }])->get();
        return response(json_encode($this->response));
    }
    public function teamIndex()
    {
        return view('team.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('team.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData=$request->all()['data'];
        $requestData=$this->validator($requestData);
        if($requestData->fails()){
            return  response(json_encode($requestData->errors()),'422');
        }
        else{
            $requestData=$requestData->validated();
        }
        $team=auth()->user()->team()->create([
            'name' => $requestData['name'],
            'email' => $requestData['email'],
            'password' => Hash::make($requestData['password']),
        ]);
        $team->assignRole($requestData['user_role']);
        return response()->json($this->response);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $this->response['result']=$user;
        return response(json_encode($this->response));
    }
    public function teamShow($id){
        return view('team.show',compact('id'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $this->response['result']=$user;
        return response(json_encode($this->response));
    }

    public function update(Request $request, User $user)
    {

        $user->update([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
        $user->assignRole($request['user_role']);
        $this->response['message']="Update Successfull";
        return response(json_encode($this->response));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect('team/');
    }
}
