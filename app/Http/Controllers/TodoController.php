<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Validation\Validator;
use \Illuminate\Http\Response;
use \Illuminate\Contracts\Routing\ResponseFactory;
class TodoController extends Controller
{
    protected $response=array("status"=>true,"result"=>"","message"=>"",'errors'=>'');
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return \Validator::make($data, [
            'todo'=>['required','array'],
            'todo.title' => ['required', 'string'],
            'todo.details' => [],
            'todo.on_date' => ['required'],//, 'confirmed' for conifrm pass
            'tasks' => ['required','array','min:1'],
            'tasks.at_time*'=>['required'],
            'tasks.title*'=>['required'],
            'tasks.details*'=>[],
            'tasks.max_min*'=>['required'],
            'tasks.user_id*'=>['required'],
        ]);
    }
    /**
     * Display a listing of the resource.
     * @return ResponseFactory|Response
     */
    public function index()
    {

        $this->response['result']=auth()->user()->todos;
        return response(json_encode($this->response));

    }
    public function todoIndex()
    {
//        dd(Auth::user());
        return view('todo.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('todo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData=$request->all()['data'];
        $requestData=$this->validator($requestData);
        if($requestData->fails()){
            return  response(json_encode($requestData->errors()),'422');
        }
        else{
            $requestData=$requestData->validated();
        }
        $todo=auth()->user()->todos()->create($requestData['todo']);
        $todo->tasks()->createMany($requestData['tasks']);
        return response()->json($this->response);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function show(Todo $todo)
    {
        $this->response['result']=array('todo'=>$todo,'tasks'=>$todo->tasks()->with('user')->get());
        return response(json_encode($this->response));
    }
    public function todoShow($id){
        return view('todo.show',compact('id'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function edit(Todo $todo)
    {
        $this->response['result']=$todo->tasks()->with(['user'=>function($query){
            $query->selet('name');
        }]);
        return response(json_encode($this->response));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $requestData=$request->all()['data'];
        $todo=Todo::find($request['id']);
        $todo->update($requestData['todo']);
        $todo->tasks()->delete();
        $todo->tasks()->createMany($requestData['tasks']);
        $this->response['message']="Update Successfull";
        return response(json_encode($this->response));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Todo $todo)
    {
        $todo->delete();
        return back()->with('flash_message','Task Updated');
    }
}
