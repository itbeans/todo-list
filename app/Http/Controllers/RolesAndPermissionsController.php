<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsController extends Controller
{

    public function index()
    {
        $roles=Role::get();
        $this->response['result']= $roles;
        return response(json_encode($this->response));
    }
}
