<?php

namespace App\Jobs;

use App\Todo;
use App\TodoTask;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateTaskStatus implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $todos=Todo::with('tasks')->where("on_date",'<=', now()->toDateString())->get();
        foreach ($todos as $todo ){
            foreach ($todo->tasks as $task){
                if(now() > Carbon::parse(Carbon::parse($task->created_at. ' '. $task->at_time)->addMinutes($task->max_min))) {
                    $task->update(['status' => 2]);
                }
            }
        }
    }
}
