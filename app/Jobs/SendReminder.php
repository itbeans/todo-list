<?php

namespace App\Jobs;

use App\Mail\ReminderEmail;
use App\Reminder;
use App\ReminderDetail;
use App\ReminderType;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendReminder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $reminderIds = ReminderDetail::where([
            ['is_done', 0],['at_date','<=', now()->toDateString()],
            ['at_time','<=', now()->toTimeString()]
         ])->pluck('reminder_id')->toArray();

        $reminders = Reminder::with(['reminder_details'=>function($query) {
            $query->where([
                ['is_done', 0],
                ['at_date','<=', now()->toDateString()],
                ['at_time','<=', now()->toTimeString()]
            ]);
        }])->with('user','todo.user','todo.tasks.user')->whereIn('id', $reminderIds)->where('status',0)->get();

        foreach ($reminders as $reminder) {
            $this->remindUsers($reminder);

        }
    }

    /**
     * @param Reminder $reminder
     */
    private function remindUsers(Reminder $reminder)
    {
        try {
            foreach ($reminder->reminder_details as $reminder_detail) {
                if (!empty($reminder->todo->tasks)) {
                    foreach ($reminder->todo->tasks as $task) {
                        if(! empty($task->user->email)) {
                            //pass to email  job $task->user->email;
                            \Log::info(' Reminder => '. $reminder->id .' Task => '.$task->id.' User Email => '. $task->user->email);
                            $title='Todo Task Reminder';
                            $details="Mr.".$task->user->name." You Have A Task ".$task->title." To Do At ".$task->time.". Assigned By ".$reminder->todo->user->name;
                            Mail::to($task->user->email)->queue(new ReminderEmail(['title'=>$title,'details'=>$details]));
                        }
                    }
                }

                \Log::info('Reminder => '. $reminder->id );

                $reminder_detail->update(['is_done' => 1]);
            }

            if ($reminder->reminder_details->count() == 1) {
                $reminder->update(['status' => 1]);
            }
            //
            $title='Todo Reminder';
            $details="Mr.".$reminder->user->name." You Have A Todo ".$reminder->todo->title;
            Mail::to($reminder->user->email)->queue(new ReminderEmail(['title'=>$title,'details'=>$details]));

        } catch (\Exception $exception) {
            \Log::error($exception->getMessage());
        }
    }
}
