<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReminderDetail extends Model
{
    //
    protected $guarded = ['at_datetime'];

    public function reminder(){
        return $this->belongsTo(Reminder::class);
    }
//    public function setAtDatetimeAttribute()
//    {
//        $this->attributes['at_datetime'] = $this->attributes['at_date'].' '.$this->attributes['at_matime'];
//    }
}
