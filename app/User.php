<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public  function todos(){
        return $this->hasMany(Todo::class);
    }
    public  function reminders(){
        return $this->hasMany(Reminder::class);
    }
    public function parent(){
        return $this->belongsTo(User::class,'parent_id');
    }
    public function team(){
        return $this->hasMany(User::class,'parent_id');
    }
    public function tasks(){
        return $this->hasMany(TodoTask::class);
    }
}
