<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class Team extends Model
{
    use HasRoles;
    protected $table="users";
    protected $guarded = ['user_role'];

    public function parent(){
        return $this->belongsTo(User::class,'parent_id');
    }
}
