<?php

namespace App\Console;

use App\Jobs\SendReminder;
use App\Jobs\UpdateTaskStatus;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $this->remindAlerts($schedule);
        $this->updateTask($schedule);
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }

    private function remindAlerts(Schedule $schedule){
        $schedule->job(SendReminder::class)->everyMinute()->name('RemindAlertEmails')->withoutOverlapping();
        $schedule->job(UpdateTaskStatus::class)->everyMinute()->name('RemindAlertEmails')->withoutOverlapping();

    }
    private function updateTask(Schedule $schedule){
        $schedule->job(UpdateTaskStatus::class)->everyMinute()->name('UpdateTask')->withoutOverlapping();
    }
}
