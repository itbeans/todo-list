<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TodoTask extends Model
{
    /**
     * mass assignment.
     *
     * @var array
     */
//    protected $attributes = [
//        'Pending' => 0,
//        'A' => 0,
//    ];
    protected $guarded = [];
    public function todo(){
        return $this->belongsTo(Todo::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public  function getStatusAttribute($attribute){
        return [0=>'Pending',1=>'In-Progress',2=>'Overdue',3=>'Done'][$attribute];
    }
}
