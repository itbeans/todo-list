-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 14, 2019 at 01:42 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_demo_todo`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_06_055340_create_jobs_table', 1),
(5, '2019_12_06_061410_create_todos_table', 1),
(6, '2019_12_06_061441_create_reminders_table', 1),
(7, '2019_12_06_061803_create_todo_tasks_table', 1),
(8, '2019_12_10_123956_create_permission_tables', 1),
(9, '2019_12_13_054322_create_reminder_details_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1),
(5, 'App\\User', 2),
(9, 'App\\User', 3),
(13, 'App\\User', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'create todo', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49'),
(2, 'edit todo', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49'),
(3, 'view todo', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49'),
(4, 'delete todo', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49'),
(5, 'create reminder', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49'),
(6, 'edit reminder', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49'),
(7, 'view reminder', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49'),
(8, 'delete reminder', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49'),
(9, 'create team', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49'),
(10, 'edit team', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49'),
(11, 'view team', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49'),
(12, 'delete team', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49');

-- --------------------------------------------------------

--
-- Table structure for table `reminders`
--

CREATE TABLE `reminders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `todo_id` int(11) NOT NULL,
  `lets_process` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reminders`
--

INSERT INTO `reminders` (`id`, `title`, `details`, `status`, `user_id`, `todo_id`, `lets_process`, `created_at`, `updated_at`) VALUES
(1, 'Reminder For Todo Today', NULL, 0, 1, 1, 0, '2019-12-14 06:23:46', '2019-12-14 06:23:46'),
(2, 'Faizi first push', NULL, 0, 1, 1, 0, '2019-12-14 07:10:40', '2019-12-14 07:10:40');

-- --------------------------------------------------------

--
-- Table structure for table `reminder_details`
--

CREATE TABLE `reminder_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `at_date` date NOT NULL,
  `at_time` time NOT NULL,
  `is_done` int(11) NOT NULL DEFAULT '0',
  `reminder_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reminder_details`
--

INSERT INTO `reminder_details` (`id`, `at_date`, `at_time`, `is_done`, `reminder_id`, `created_at`, `updated_at`) VALUES
(1, '2018-01-15', '05:23:00', 0, 1, '2019-12-14 06:23:46', '2019-12-14 06:23:46'),
(2, '2018-01-15', '07:11:00', 0, 1, '2019-12-14 06:23:46', '2019-12-14 06:23:46'),
(3, '2018-01-05', '08:24:00', 0, 1, '2019-12-14 06:23:46', '2019-12-14 06:23:46'),
(4, '2018-03-15', '06:09:00', 0, 2, '2019-12-14 07:10:40', '2019-12-14 07:10:40');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'super admin', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49'),
(2, 'super editor', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49'),
(3, 'super viewer', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49'),
(4, 'super writer', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49'),
(5, 'todo admin', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49'),
(6, 'todo editor', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49'),
(7, 'todo viewer', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49'),
(8, 'todo writer', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49'),
(9, 'reminder admin', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49'),
(10, 'reminder editor', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49'),
(11, 'reminder viewer', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49'),
(12, 'reminder writer', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49'),
(13, 'team admin', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49'),
(14, 'team editor', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49'),
(15, 'team viewer', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49'),
(16, 'team writer', 'web', '2019-12-14 04:55:49', '2019-12-14 04:55:49');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(1, 4),
(1, 5),
(1, 6),
(1, 8),
(2, 1),
(2, 2),
(2, 4),
(2, 5),
(2, 6),
(2, 8),
(3, 1),
(3, 2),
(3, 3),
(3, 4),
(3, 5),
(3, 6),
(3, 7),
(3, 8),
(4, 1),
(4, 4),
(4, 5),
(4, 8),
(5, 1),
(5, 2),
(5, 4),
(5, 9),
(5, 10),
(5, 12),
(6, 1),
(6, 2),
(6, 4),
(6, 9),
(6, 10),
(6, 12),
(7, 1),
(7, 2),
(7, 3),
(7, 4),
(7, 9),
(7, 10),
(7, 11),
(7, 12),
(8, 1),
(8, 4),
(8, 9),
(9, 1),
(9, 2),
(9, 4),
(9, 13),
(9, 14),
(9, 16),
(10, 1),
(10, 2),
(10, 4),
(10, 13),
(10, 14),
(10, 16),
(11, 1),
(11, 2),
(11, 3),
(11, 4),
(11, 13),
(11, 14),
(11, 15),
(11, 16),
(12, 1),
(12, 4),
(12, 13),
(12, 16);

-- --------------------------------------------------------

--
-- Table structure for table `todos`
--

CREATE TABLE `todos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `on_date` date DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `todos`
--

INSERT INTO `todos` (`id`, `title`, `details`, `on_date`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Todays Todo For Team', NULL, '2019-12-14', 1, '2019-12-14 05:41:45', '2019-12-14 05:41:45'),
(2, 'Todays Todo For Team SAS', NULL, '2019-12-15', 1, '2019-12-14 05:43:30', '2019-12-14 05:43:30'),
(3, 'sadsadsad', NULL, '2018-01-15', 1, '2019-12-14 07:42:13', '2019-12-14 07:42:13');

-- --------------------------------------------------------

--
-- Table structure for table `todo_tasks`
--

CREATE TABLE `todo_tasks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `at_time` time DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `todo_id` int(11) NOT NULL,
  `max_min` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `todo_tasks`
--

INSERT INTO `todo_tasks` (`id`, `at_time`, `title`, `details`, `todo_id`, `max_min`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
(1, '15:38:00', 'Do This', 'bla bla bla...', 1, 30, 2, 0, '2019-12-14 05:41:45', '2019-12-14 05:41:45'),
(2, '16:39:00', 'Do That', 'bla.. bla.. bla..', 1, 30, 3, 0, '2019-12-14 05:41:45', '2019-12-14 05:41:45'),
(3, '17:40:00', 'Review Do This And That', 'bla..bla..baa', 1, 30, 4, 0, '2019-12-14 05:41:45', '2019-12-14 05:41:45'),
(4, '04:43:00', 'Do What', 'bla...bla...bla', 2, 30, 2, 0, '2019-12-14 05:43:30', '2019-12-14 05:43:30'),
(5, '05:42:00', 'Do What Ever You Want', 'bla...bla...bla', 2, 30, 3, 0, '2019-12-14 05:43:30', '2019-12-14 05:43:30'),
(6, '06:42:00', 'ssadasd', 'sadsad', 3, 30, 2, 0, '2019-12-14 07:42:13', '2019-12-14 07:42:13');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '0 for admin',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `parent_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'faizee', 'mr.shakir117@gmail.com', NULL, '$2y$10$qxh9faoCQNvRm.wYxMYwqeJM9Zfa7E4acU52c4cZLxMSCcYaLaLoS', 0, NULL, '2019-12-14 05:33:46', '2019-12-14 05:33:46'),
(2, 'suleman afzal', 'sulemanafzal5@gmail.com', NULL, '$2y$10$PeL9Z25CvFleT0/oKzJ5h./WdDjlPHi4c9gKmRQXWU9g804fEFB7O', 1, NULL, '2019-12-14 05:37:28', '2019-12-14 05:37:28'),
(3, 'Shikeh Hanan', 'abdul_hanan@it-beans.com', NULL, '$2y$10$UIYkw.3rxRPGqvr281hPde9Q/YL8itcuNQ3QgCWP1tNxV//EIHeUa', 1, NULL, '2019-12-14 05:38:10', '2019-12-14 05:38:10'),
(4, 'Shah', 'shah@it-beans.com', NULL, '$2y$10$D9CAVsBP58.eHN7yWKfBOuzhw7Uhf5QWgpUbP7N3hgSUdvtfhPcKi', 1, NULL, '2019-12-14 05:38:57', '2019-12-14 05:38:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reminders`
--
ALTER TABLE `reminders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reminder_details`
--
ALTER TABLE `reminder_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `todos`
--
ALTER TABLE `todos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `todo_tasks`
--
ALTER TABLE `todo_tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `reminders`
--
ALTER TABLE `reminders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `reminder_details`
--
ALTER TABLE `reminder_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `todos`
--
ALTER TABLE `todos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `todo_tasks`
--
ALTER TABLE `todo_tasks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
